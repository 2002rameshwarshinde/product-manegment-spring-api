package org.dnyanyog.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class Product {
	public String productName;
	public Double price;
	public Integer Quantity;
	
	
	@Autowired
	public Category category;
	
	@Autowired
	public Variations variations;
	
	public void printProductDetails()
	{
		System.out.println("Product [productName=" + productName + ",price=" + price + "," + " quantity=" + Quantity + "]; Category[" + category +"]; Variations [ "+variations+"];");

	}
}
