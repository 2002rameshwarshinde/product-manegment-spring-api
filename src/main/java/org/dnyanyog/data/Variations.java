package org.dnyanyog.data;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class Variations {
	   @Autowired
		public List <String>variationList;
		
		public String toString () {
			StringBuilder sb=new StringBuilder();
			variationList.forEach(str -> sb.append(str+""));
			return sb.toString();
			
		}
		public void add(String VariationName) {
			variationList.add(VariationName);
		}
}
