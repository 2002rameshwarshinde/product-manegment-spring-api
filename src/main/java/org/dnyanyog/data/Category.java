package org.dnyanyog.data;

import org.springframework.stereotype.Component;

@Component
public class Category {
	public String name;
	public String toString () {
		return "Category[name="+name+"]";
	}
}
