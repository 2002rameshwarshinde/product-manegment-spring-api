package org.dnyanyog;

import java.util.ArrayList;

import org.dnyanyog.data.Product;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class ApplicationMain {
	public static void main(String []args) {
		ConfigurableApplicationContext	context=SpringApplication.run(ApplicationMain.class, args);
			
		Product product=context.getBean(Product.class);
		product.productName="pen";
		product.price=(double) 5;
		product.Quantity=60;
		product.category.name="Stationary";
		product.variations.variationList=new ArrayList<String>();
		product.variations.variationList.add("Red Color");
		product.variations.variationList.add("Blue Color");
		product.variations.variationList.add("green Color");
		product.printProductDetails();
		}
}
